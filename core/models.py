from django.db import models
from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.urls import reverse

# Create your models here.
class CustomUser(AbstractUser):
    user_type_choices=((1,"Pharmacy Owner"),(2," Phermacist"),(3,"Customer"))
    user_type=models.CharField(max_length=255,choices=user_type_choices,default=1)
    # role = models.CharField("User Type", max_length=10, choices=USER_TYPE, default='Customer')

class PharmacyOwnerProfile(models.Model):
    id=models.AutoField(primary_key=True)
    user = models.OneToOneField(CustomUser, blank=True, null=True, on_delete=models.SET_DEFAULT, default=None)
    mobileNo = models.CharField(max_length=40)
    cnic = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    address = models.CharField(max_length=30)
    shop_name = models.CharField(max_length=30)
    objects=models.Manager()

    def __str__(self):
        return self.user.username


class PharmacistProfile(models.Model):
    id=models.AutoField(primary_key=True)
    user = models.OneToOneField(CustomUser, models.SET_DEFAULT, default=None)
    gender = models.CharField(max_length=255)
    profile_pic = models.FileField()
    address = models.TextField()
    country = models.CharField(max_length=255, default=None)
    province = models.CharField(max_length=255, default=None)
    district = models.CharField(max_length=255, default=None)
    city = models.CharField(max_length=255, default=None)
    zip_code = models.CharField(max_length=255, default=None)
    phone_number = models.CharField(max_length=255, default=None)
    education = models.CharField(max_length=255, default=None)
    workplace = models.CharField(max_length=255, default=None)
    objects=models.Manager()


    def __str__(self):
        return self.user.username

class CustomerProfile(models.Model):
    id=models.AutoField(primary_key=True)
    user = models.OneToOneField(CustomUser, models.SET_DEFAULT, default=None)
    objects=models.Manager()


    def __str__(self):
        return self.user.username




class Pharmacy(models.Model):
    id=models.AutoField(primary_key=True)
    name=models.CharField(max_length=255)
    license_no=models.CharField(max_length=255)
    address=models.CharField(max_length=255)
    contact_no=models.CharField(max_length=255)
    email=models.CharField(max_length=255)
    description=models.CharField(max_length=255)
    added_on=models.DateTimeField(auto_now_add=True)
    objects=models.Manager()

class PharmacyBranch(models.Model):
    id=models.AutoField(primary_key=True)
    name_of_pharmacy = models.CharField(max_length=255)
    location_address = models.CharField(max_length=255)
    country = models.CharField(max_length=255, default=None)
    province = models.CharField(max_length=255, default=None)
    district = models.CharField(max_length=255, default=None)
    city = models.CharField(max_length=255, default=None)
    zip_code = models.CharField(max_length=255, default=None)
    phone_number = models.CharField(max_length=255, default=None)
    license_no = models.CharField(max_length=255)
    license_operate = models.FileField()
    health_safety_code = models.CharField(max_length=255)
    about = models.TextField(max_length=150)
    website = models.CharField(max_length=255)
    contact_no=models.CharField(max_length=255)
    email=models.CharField(max_length=255)
    description=models.CharField(max_length=255)
    added_on=models.DateTimeField(auto_now_add=True)
    objects=models.Manager()

class Product(models.Model):
    id=models.AutoField(primary_key=True)
    name=models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    unit = models.CharField(max_length=255)
    price=models.IntegerField()
    gross=models.IntegerField()
    size=models.CharField(max_length=255)
    strength=models.CharField(max_length=255)
    instock=models.IntegerField()
    reader_limit=models.CharField(max_length=255)
    expire_date=models.DateField()
    added_by_pharmacist=models.ForeignKey(PharmacistProfile,on_delete=models.CASCADE)
    mfg_date=models.DateField()
    description=models.CharField(max_length=255)
    attention=models.CharField(max_length=255)
    frequecy=models.CharField(max_length=255)
    composition=models.CharField(max_length=255)
    notes=models.CharField(max_length=255)
    added_on=models.DateTimeField(auto_now_add=True)
    edited_on=models.DateTimeField(auto_now_add=True)
    objects=models.Manager()

class ProductMedia(models.Model):
    id=models.AutoField(primary_key=True)
    product_id=models.ForeignKey(Product,on_delete=models.CASCADE)
    media_type_choice=((1,"Image"),(2,"Video"))
    media_type=models.CharField(max_length=255)
    media_content=models.FileField()
    created_at=models.DateTimeField(auto_now_add=True)
    is_active=models.IntegerField(default=1)

class ProductTransaction(models.Model):
    id=models.AutoField(primary_key=True)
    transaction_type_choices=((1,"BUY"),(2,"SELL"))
    product_id=models.ForeignKey(Product,on_delete=models.CASCADE)
    transaction_product_count=models.IntegerField(default=1)
    transaction_type=models.CharField(choices=transaction_type_choices,max_length=255)
    transaction_description=models.CharField(max_length=255)
    created_at=models.DateTimeField(auto_now_add=True)

class CustomerOrders(models.Model):
    id=models.AutoField(primary_key=True)
    product_id=models.ForeignKey(Product,on_delete=models.DO_NOTHING)
    purchase_price=models.CharField(max_length=255)
    coupon_code=models.CharField(max_length=255)
    discount_amt=models.CharField(max_length=255)
    product_status=models.CharField(max_length=255)
    created_at=models.DateTimeField(auto_now_add=True)

class OrderDeliveryStatus(models.Model):
    id=models.AutoField(primary_key=True)
    order_id=models.ForeignKey(CustomerOrders,on_delete=models.CASCADE)
    status=models.CharField(max_length=255)
    status_message=models.CharField(max_length=255)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)

class Activity(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=255)
    item = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    delivered = models.IntegerField(default=1)
    quantity = models.IntegerField()
    total = models.IntegerField()
    costofdelivery = models.IntegerField()
    delivery_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()

    def __str__(self):
        return self.name

class Messages(models.Model):
    id = models.AutoField(primary_key=True)
    sender = models.CharField(max_length=255)
    reciever = models.CharField(max_length=255)
    subject = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()

    def __str__(self):
        return f'{self.sender} sending message to {self.reciever}'

class NotificationPharmacist(models.Model):
    id = models.AutoField(primary_key=True)
    pharmacist_id = models.ForeignKey(PharmacistProfile, on_delete=models.CASCADE)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()


class NotificationCustomer(models.Model):
    id = models.AutoField(primary_key=True)
    customer_id = models.ForeignKey(CustomerProfile, on_delete=models.CASCADE)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()


class Prescription(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.CharField(max_length=255)
    patient = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    quantity = models.IntegerField()
    price = models.IntegerField()
    patient_contact = models.CharField(max_length=255)
    presciber_name = models.CharField(max_length=255)
    presciber_contact = models.CharField(max_length=255)
    date = models.DateField(auto_now_add=True)
    objects = models.Manager()

    def __str__(self):
        return self.name

class Report(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    objects = models.Manager()

    def __str__(self):
        return self.name

class Advertisement(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    objects = models.Manager()

    def __str__(self):
        return self.name


@receiver(post_save,sender=CustomUser)
def create_user_profile(sender,instance,created,**kwargs):
    if created:
        if instance.user_type==1:
            PharmacyOwnerProfile.objects.create(user=instance)
        if instance.user_type==2:
            PharmacistProfile.objects.create(user=instance)
        if instance.user_type==3:
            CustomerProfile.objects.create(user=instance)

@receiver(post_save,sender=CustomUser)
def save_user_profile(sender,instance,**kwargs):
    if instance.user_type==1:
        instance.pharmacyownerprofile.save()
    if instance.user_type==2:
        instance.phermacistprofile.save()
    if instance.user_type==3:
        instance.customerprofile.save()